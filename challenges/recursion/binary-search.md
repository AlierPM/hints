# Recursion: Binary Search 

<!-- Hint -->
<details>
  <summary><b>Hint:</b>&nbsp;Basis of the problem</summary><br>

Previously, we made a basic introduction to recursion topic. We used recursive approach to handle repetition on problem solving, and introduced the Divide & Conquer approach.

Every possible recursive implementation can be handled by a iterative one. At the same time, iterative solutions are routinely more efficient on resource usage(recursion suffers from function call overhead).

Accordingly, we can ask some questions, and we should.

Why should we use recursion?<br>
&emsp;\- recursion is more elegant, and mostly more readable<br>
&emsp;\- recursive call stack handles some programming complexities(for example, supports immutability)<br>

Or the more straight of it, when should we use recursion?<br>
&emsp;\- some problems are easier to decribe with it<br>
&emsp;\- when there are needed multiple calls(branching)<br>
&emsp;\- when it's less complex than the iterative one<br>

---

The binary search algorithm takes advantage of nearly all the benefits listed above. That's why, it's accepted as naturally a recursive algorithm.

Basically, the binary search finds the position of a target value within a **sorted array** by following the Divide & Conquer approach.

At the each recursive step, it compares the searched value to the middle element of the current range. Then, it repeatedly divides the array into half(called half-interval search as well) and conquers each range separately(no need to combine any recursive call, the solution is trivial).

There're three fundamental cases:<br>
&emsp;\- if the target value equals to the middle element, the middle interval holds the square root. Simply, return it.<br>
&emsp;\- if the target value is lesser than the middle element, do binary search for the range after the middle interval<br>
&emsp;\- if the target value is greater than the middle element, do binary search for the range before the middle interval<br>

<img src="images/recursion/binary-search/binary-search-1.png"  width="800"><br>

A basic implementation is:<br>
```ruby
def binary_search(target, array, min_interval = 0, max_interval = array.length - 1)
  # compute middle index from the range
  mid_interval = (min_interval + max_interval) / 2
  mid_value = array[mid_interval]
  # if the middle element equals to the target, we found it
  if mid_value == target
    mid_interval
  # if the middle element is greater than the target value
  # do a binary search before the middle index
  elsif mid_value > target
    binary_search(target, array, min_interval, mid_interval)
  # if the middle element is lesser than the target value
  # do binary search after the middle index
  else
    binary_search(target, array, mid_interval + 1, max_interval)
  end
end
```

---

<br>

In this challenge, we'll do binary search to find the square root of the given number.

But as opposed to typical binary search, there is neither an array nor a target value given from parameters. So, we need to distill the required information from the given number.

We can estimate that the square root of the number lies in range from zero to the given number(`0 <= square_root <= number`). So, let's assume that there is an array of numbers from zero to the given number sequentially.

<img src="images/recursion/binary-search/binary-search-2.png"  width="800"><br>

<b>`Hint:`&nbsp;For the initial call, use a range starts from 0 up to the given number.<br>
<b>`Hint:`&nbsp;Implement a binary search algorithm to find the a value which its square equals to the given number.<br>

<b>`Note:`</b>&nbsp;No need to work on an actual array. The problem can be solved by using only the intervals.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint:</b>&nbsp;The required cases for the recursion</summary><br>

Below illustration explains how-to do the binary search for finding the square root of "9".

<img src="images/recursion/binary-search/binary-search-3.png"  width="800"><br>

The required flow can write down as:<br>
<b>`Hint:`&nbsp;Compute the middle interval<br>
<b>`Hint:`&nbsp;Then, compute the square of the middle interval<br>
<b>`Hint:`&nbsp;If it equals to the given number, we found the square root. Simply return middle interval.<br>
<b>`Hint:`&nbsp;If it's greater than the given number, do the binary search for the range before the middle interval.<br>
<b>`Hint:`&nbsp;If it's lesser than the given number, do the binary search for the range after the middle interval.<br>
  
</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]:</b>&nbsp;Solution(recursive)</summary><br>

```ruby
def sqrt(number)
  sqrt_recursive(number, 0, number)
end

def sqrt_recursive(number, min_interval, max_interval)
  mid_interval = (max_interval + min_interval) / 2
  mid_square = mid_interval * mid_interval

# if it's the square root, return middle interval
  if mid_square == number
    return mid_interval
  elsif mid_square > number
    # do the binary search for the range before the middle interval
    sqrt_recursive(number, min_interval, mid_interval)
  else
      # do the binary search for the range after the middle interval
    sqrt_recursive(number, mid_interval + 1, max_interval)
  end
end

```
  
</details>
<!-- Solution -->

